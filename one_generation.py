###############################################################
# A simulation routine for one generation (of several bikes?) #
###############################################################

"""
What this module does:
1) Instantiates a group of bikes with predefined property states
2) Progress through time using physics module
3) Terminates with a triger
4) Returns the two best bike objects. 
"""

import bikes, bike_animation 
import numpy as np
import matplotlib.pyplot as plt, matplotlib.animation as animation

# A new generation comes to the world (say 10 individuals)
bike = [] 
for i in range(1):
    wheel1 = bikes.Vertex(radius = np.random.rand() + 0.2,
                         mass = np.random.rand(),
                         x = 2 * np.random.rand(),
                         y = 10 + np.random.rand(),
                         omega = 1e-1 * np.random.rand()
                         )
    wheel2 = bikes.Vertex(radius = np.random.rand() + 0.2,
                         mass = np.random.rand(),
                         x = 2 * np.random.rand(),
                         y = 10 - np.random.rand(),
                         omega = 0
                         )
    mass1 = bikes.Vertex(radius = 0,
                         mass = np.random.rand(),
                         x = 2 * np.random.rand(),
                         y = 10 - np.random.rand(),
                         omega = 0
                         )
    mass2 = bikes.Vertex(radius = 0,
                         mass = np.random.rand(),
                         x = 2 * np.random.rand(),
                         y = 10 - np.random.rand(),
                         omega = 0
                         )
    spring_w1w2 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = wheel1,
                               v2 = wheel2,
    )
    spring_w1m1 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = wheel1,
                               v2 = mass1,
                          )
    spring_w1m2 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = wheel1,
                               v2 = mass2,
                          )
    spring_w2m1 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = wheel2,
                               v2 = mass1,
                          )
    spring_w2m2 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = wheel2,
                               v2 = mass2,
                          )
    spring_m1m2 = bikes.Spring(l0 = np.sqrt((wheel1.x - wheel2.x)**2
                                       + (wheel1.y - wheel2.y)**2),
                               k = 1 * np.random.rand() + 1e-2,
                               v1 = mass1,
                               v2 = mass2,
                          )
    newbike = bikes.Bike(wheel1, wheel2, mass1, mass2,
                         spring_w1w2, spring_w1m1, spring_w1m2, spring_w2m1,
                         spring_w2m2, spring_m1m2)
    bike.append(newbike)

# Bikes struggle forward
figure = plt.figure()

animate_bikes = bike_animation.AnimateBikes(figure, bike,
                                            timestep = 0.006)
ani = animation.FuncAnimation(figure,
                              animate_bikes,
                              np.arange(1500),
                              init_func=animate_bikes.init_frame,
                              interval=10,
                              repeat = False,
)
##ani.save('single_bike.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()

