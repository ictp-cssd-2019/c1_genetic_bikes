How to use ``genetic_bikes``
----------------------------

Requirements
============

``genetic_bikes`` is written for ``Python 3`` and requires:

- numpy
- matplotlib plus a backend of your choice (``TkAgg`` by default)


Installation
============

Download and unpack the source into a directory of your choice.

Usage
=====

Start the simulation with ``python3 genetic_bikes.py`` or ``%run
genetic_bikes.py`` from inside ``ipython``. 
  
