About ``genetic_bikes``
----------------------

In ``genetic_bikes``, we create a simulation in Python to evolve an optimal
bicycle frame.

Our model has the following parameters:

- Geometry:
  The simulation is restricted to two dimensions: distance x
  and height h (plus time t of course).
- Bicycle:
  The bike consists of four vertices. Two of them represent
  wheels of an arbitrary size and small mass. The other two are more
  massive points that simulate the load of the rider on the frame. One
  of the wheels drives the frame. The four vertices are fully
  connected by six springs, with variable rest length and spring
  constants.
- Ground:
  For this simple case, the ground will be flat.
- Physics:
  All movement comes from the laws of mechanics. We
  assume that the driving wheel provides a constant acceleration and
  does not slip.
- Evolution:
  The final shape of the bike is not hardcoded. Starting
  from random initial configurations, the final shape emerges through
  an evolutionary process of competitive selection. The fitness of a
  bike will be measured by the distance it travels after being dropped
  to the ground.
- Visualization:
  Each bike’s movement along the road in the evolution
  process is shown as a matplotlib animation.
