Implementation Details
----------------------

.. toctree::
   :maxdepth: 2
   :caption: Modules:

   implementation/bikes
   implementation/physics
   implementation/genetic_algorithm
   implementation/animation
