.. genetic_bikes documentation master file, created by
   sphinx-quickstart on Tue May  7 16:58:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to genetic_bikes's documentation!
=========================================

In ``genetic_bike``, we create a simulation in Python to evolve an
optimal bicycle frame.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   usage
   implementation
   
                
             

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
