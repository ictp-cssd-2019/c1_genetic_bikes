The ``physics`` module
----------------------

All movement comes from the laws of mechanics. We assume that the
  driving wheel provides a constant acceleration and does not slip.

.. automodule:: physics
   :members:
