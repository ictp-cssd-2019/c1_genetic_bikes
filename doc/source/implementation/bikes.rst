The ``bikes`` module
--------------------

The bike consists of four vertices. Two of them represent wheels of an
  arbitrary size and small mass. The other two are more massive points
  that simulate the load of the rider on the frame. One of the wheels
  drives the frame. The four vertices are fully connected by six
  springs, with variable rest length and spring constants.

.. automodule:: bikes
   :members:

      
