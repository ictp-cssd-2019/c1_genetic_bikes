The ``bike_animation`` module
-----------------------------

Each bike’s movement along the road in the evolution
  process is shown as a matplotlib animation.

.. automodule:: bike_animation
   :members:

