
def force_calculator(list_of_object, index):#obj= object for example mass1 or wheel2
    """
    this function calculates the summation of the connected springs force
    """
    Fx=0
    Fy=0
    
    obj = list_of_object[index]
    for i in range(int(len(obj[7])/3)):
        dX=(list_of_object[int(obj[7][3*i])][3]-obj[3])   #obj[7][3*i][3]=connections[3*i][3]
        dY=(list_of_object[int(obj[7][3*i])][4]-obj[4])
        l=(dX**2+dY**2)**0.5 # l is the current distance between two objects
        F=obj[7][3*i+1]*(l-obj[7][3*i+2]) # spring constant * (l-l_equilibrium)
        Fx+=F*dX/l
        Fy+=F*dY/l
    return([Fx,Fy])

def evelutioner(list_of_object, index, dt):
    
    """
    this function takes an element (for example one wheel) and evaluat the new location of it
    the input is the list of a wheel for example
    
    """
    g=9.8
    element = list_of_object[index]
    e=element[0]/10
    F=force_calculator(list_of_object, index)
    m=element[1]
    Vx=element[5]
    Vy=element[6]
    Ax=F[0]/m
    Ay=(F[1]/m)-g
    element[5]+=Ax*dt
    element[6]+=Ay*dt
    element[3]+=(Vx+Ax*dt/2)*dt
    element[4]+=(Vy+Ay*dt/2)*dt
    if element[4]<element[0]+e :
        if element[6]<0: 
            element[6]*=-0.3
        element[5]+=abs((element[0]*element[2]))/m
    return()
    
def physics (list_of_object,dt):#list_of_object=[wheel1,wheel2]
    
    '''
    
    physics is a function that operates on the objects (wheels and masses)
    wheel is a list that composed of characteristics of the wheel
    the form of the wheel: wheel=[R,m,omega,Xcm,Ycm,Vx,Vy,connections]
        connections: is a list that contains of the index of object in the list_of_object that connected to that wheel and also the hardness and free length of the connecting spring
        example of connections= [0,k1,l01,2,k2,l02,1,k3,l03,...] (k1 and l01 for wheel1 ,...)
        R is the radious of the wheel
        m the mass of the wheel
        omega is the angular velocity 
        Xcm and Ycm is the possition of the central mass of the wheel
        Vx and Yx is the velocity of the central 
     dVx=abs(R*omega-Vx)/m   =amount of velocity that added to the wheel if it interact to the ground(accelaration)
        the form of dVx contains of dissipation 
    g= gravity acceleration
    dt= time step
    e= limit of interact with the ground
    mass is a list: mass=[0,m,0,Xcm,Ycm,Vx,Vy,connections]
    example of connections= [0,k1,l01,2,k2,l02,1,k3,l03,...] (k1 and l01 for wheel1 ,...)
    example of list_of_object:
        list_of_object=[[3,2,1,12,3,0,0,[1,4,4]],[2,2,2,15,3,0,0,[0,4,4]]]
        
    '''
    for i, element in enumerate(list_of_object):
        evelutioner(list_of_object, i, dt)
    
    
    return(list_of_object)
