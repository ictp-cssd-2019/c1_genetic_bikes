import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.collections import PatchCollection

import physics
import bikes as bike_class

"""This module creates an animation of the movement of bikes"""

class AnimateBikes(object):
    """Takes care of setup and update of the bike animation using a
    matplotlib function animation. 
    
    Bikes consist of circular patches connected by lines.  
    """
    def __init__(self, figure, bike_list, timestep):
        """"Initialise the plot frame for the animation. 
        
        The animation object will hold a number of bikes in a list
        (self.bike_list) and the patch handles corresponding to those
        bikes in self.patches, which is a list of dictionaries. 
        """
        # set up plot
        self.fig = figure
        self.ax = self.fig.add_subplot('111',aspect='equal')
        self.ax.set_ylim(0, 15)
        self.ax.set_xlim(-10, 10)
        
        # set up the animation
        self.timestep = timestep
        
        self.bike_list = bike_list
        
        # add flat ground
        self.ax.axhline(0)
        
        
    def init_frame(self):
        """
        Initialise the first frame of each generation by drawing all the
        bikes. 
        """
        for bike in self.bike_list:
            b_handle = self.draw_bike(bike)
            bike.patches = b_handle
            
        
        
        
    def draw_bike(self, bike):
        """
        Draw our primitive bike as two circles with a line connecting them.
        returns: a dictionary of handles for the wheels and the
        connecting line.
        
        returns: dictionary of handles for this bike. 
        """
        bike_handles = {}
        for key in bike.__dict__:
            if 'wheel' in key:
                wheel = bike.__getattribute__(key)
                # color rolling wheel in red
                if wheel.omega > 0:
                    color = 'r'
                else:
                    color = 'k'
                
                b = plt.Circle((wheel.x,
                                wheel.y,),
                               radius = wheel.radius,
                               fill = False,
                               edgecolor = color)
                b_handle = self.ax.add_patch(b)
                bike_handles[key] = b_handle
            elif 'mass' in key:
                mass = bike.__getattribute__(key)
                b = plt.Circle((mass.x,
                                mass.y,),
                               radius = 0.1,
                               facecolor = 'k',
                               edgecolor = 'k')
                b_handle = self.ax.add_patch(b)
                bike_handles[key] = b_handle
            elif 'spring' in key:
                # plot spring
                spring = bike.__getattribute__(key)
                s_line, = plt.plot([spring.v1.x, spring.v2.x],
                                   [spring.v1.y, spring.v2.y]
                )
                bike_handles[key] = s_line
        return bike_handles
    
    def bike_add_names(self, bike):
        """
        
        add a member ``name`` to all vertices of the bike object
        that holds its name. Necessary to find the connections. 
        
        """
        for key in bike.__dict__:
            # add names to the vertices to be available in the springs
            if 'wheel' in key or 'mass' in key or 'spring' in key:
                bike.__getattribute__(key).connections = set()
                bike.__getattribute__(key).name = key
                
        return bike
    
    def bike_add_connections(self, bike):
        """
        
        add a member ``connections`` to all vertices of the bike object
        that holds all connections to it (for now: a list of spring
        objects). See the bikes module for details.
        
        """
        for key in bike.__dict__:
            # add connections to vertices 
            if 'spring' in key:
                spring = bike.__getattribute__(key)
                vertex_keys = [spring.v1.name, spring.v2.name]
                v1 = bike.__getattribute__(vertex_keys[0])
                v1.connections.add(spring)
                v2 = bike.__getattribute__(vertex_keys[1])
                v2.connections.add(spring)
        return bike
    
    def bike_to_physics_format(self, bike):
        """
        
        Convert the bike object to the format the input format suitable
        for the physics.physics function. 
        
        The input list for the physics module is constructed in the
        following order:
        [wheel1_lst, wheel2_lst, mass1_lst, mass2_lst]
        
        The indices for connecting objects are then:
        0: wheel1
        1: wheel2
        2: mass1
        3: mass2
        
        """
        physics_lst = []
        connection_indices = {'wheel1': 0,
                              'wheel2': 1,
                              'mass1': 2,
                              'mass2': 3,
                              }
        
        def connection_index(vertex, spring):
            """
            get the index of the other vertex connected by a given spring
            """
            if vertex.name == spring.v1.name:
                return connection_indices[spring.v2.name]
            elif vertex.name == spring.v2.name:
                return connection_indices[spring.v1.name]
            
        def vertex_lst(vertex):
            connection_lst = []
            for spring in vertex.connections:
                index = connection_index(vertex, spring)
                connection_lst.extend([index, spring.k, spring.l0])
            
            v_lst = [vertex.radius, vertex.mass, vertex.omega,
                     vertex.x, vertex.y, vertex.vx, vertex.vy,
                     connection_lst
            ]
            return v_lst
        
        for vertex in [bike.wheel1, bike.wheel2, bike.mass1, bike.mass2]:
            lst = vertex_lst(vertex)
            physics_lst.append(lst)
        return physics_lst
                    
        
    def bike_updates(self):
        """
        Use the physics module to update the positions of all the 
        bikes. This requires some conversion to the input format of 
        the physics function, done in bike_to_physics_format. 
        """
        
        for bike in self.bike_list:
            self.bike_add_names(bike)
            self.bike_add_connections(bike)
            physics_lst = self.bike_to_physics_format(bike)
            
            wh1_lst, wh2_lst, m1_lst, m2_lst = physics.physics(physics_lst,
                                                               self.timestep)
            wh1 = bike.wheel1
            wh1.x, wh1.y, wh1.vx, wh1.vy = wh1_lst[3:7]
            wh2 = bike.wheel2
            wh2.x, wh2.y, wh2.vx, wh2.vy = wh2_lst[3:7]
            m1 = bike.mass1
            m1.x, m1.y, m1.vx, m1.vy = m1_lst[3:7]
            m2 = bike.mass2
            m2.x, m2.y, m2.vx, m2.vy = m2_lst[3:7]
            
            
    
    def __call__(self, frame_number):
        """Update the position of the ball 
        and slide the window to follow its movement.
        """
        # update bike positions
        self.bike_updates()
        
        # update patch coordinates 
        for b in self.bike_list:
            b.patches['wheel1'].center = (b.wheel1.x,
                                          b.wheel1.y)
            b.patches['wheel2'].center = (b.wheel2.x,
                                          b.wheel2.y)
            b.patches['mass1'].center = (b.mass1.x,
                                          b.mass1.y)
            b.patches['mass2'].center = (b.mass2.x,
                                          b.mass2.y)
            for key in b.__dict__:
                if 'spring' in key:
                    spr = b.__getattribute__(key)
                    b.patches[key].set_data([spr.v1.x, spr.v2.x],
                                            [spr.v1.y, spr.v2.y]
                                         )
            
        fig_width = self.fig.get_figwidth()
        biggest_progress = max([min(b.wheel1.x, b.wheel2.x) for b in self.bike_list])
        self.ax.set_xlim(biggest_progress - fig_width,
                         biggest_progress + fig_width)
        
        return self.bike_list,





if __name__ == "__main__":
    figure = plt.figure()
    bike = [bikes.Bike(np.random.rand(), np.random.rand(), y1 = 8, omega1 = 1) for i in range(2)]
    
    animate_bikes = AnimateBikes(figure, bike)
    
    ani = animation.FuncAnimation(figure,
                                  animate_bikes,
                                  np.arange(1000),
                                  init_func=animate_bikes.init_frame,
                                  interval=250
    )
    plt.show()


