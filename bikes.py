#################################
# Implementation of bikes class #
#################################

import numpy as np

class Bike(object):
    """
    Implementation of a 2-wheel bike:
    wheel1 (object)
    wheel2 (object)
    6 springs (objects)
    """
    def __init__(self, wheel1, wheel2,
                       mass1, mass2,
                       spring_w1w2, spring_w1m1, spring_w1m2, spring_w2m1, spring_w2m2, spring_m1m2):
        self.wheel1 = wheel1
        self.wheel2 = wheel2
        self.mass1 = mass1
        self.mass2 = mass2
        self.spring_w1w2 = spring_w1w2
        self.spring_w1m1 = spring_w1m1
        self.spring_w1m2 = spring_w1m2
        self.spring_w2m1 = spring_w2m1
        self.spring_w2m2 = spring_w2m2
        self.spring_m1m2 = spring_m1m2        
    
class Vertex(object):
    """
    Definition of a vertex. All property fields are floats. Could be wheel, could be a point mass.
    """
    def __init__(self, radius, mass, x, y, vx=0., vy=0., omega=0.):
        self.radius = radius
        self.mass = mass
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.omega = omega

class Spring(object):
    """
    Definition of a spring, with l0 as length at rest (float), k as force constant (float), and two vertices (objects).
    """
    def __init__(self, l0, k, v1, v2):
        self.l0 = l0 # length at rest
        self.k = k # force constant
        self.v1 = v1 # vertex 1
        self.v2 = v2 # vertex 2
